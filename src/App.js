import avatar from "./assets/images/avatar.jpg";

function App() {
  return (
    <div style={{width: "800px", border: "1px solid #ddd", margin: "0 auto", textAlign: "center", marginTop: "100px", padding: "0px 0px 50px 0px"}}>
      <div>
        <img src={avatar} alt="avatar" style={{borderRadius: "50%", width: "100px", marginTop: "-50px"}}></img>
      </div>
      <div>
        <p style={{fontSize: "18px"}}>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div style={{fontSize: "15px"}}>
        <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;
